package restservice.controller;

import org.apache.log4j.Logger;
import restservice.entities.House;
import restservice.repository.HouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/houses")
public class HouseController {

    final static Logger logger = Logger.getLogger(HouseController.class);

    @Autowired
    private HouseRepository houseRepository;

    @GetMapping("/{id}")
    public House getHouse(@PathVariable Long id) {
        logger.info("GET /houses/" + id);
        return houseRepository.findOne(id);
    }

    @GetMapping
    public Iterable<House> getHouses() {
        logger.info("GET /houses/all");
        return houseRepository.findAll();
    }


    @PostMapping
    public House postHouse(@RequestBody House house) {
        logger.info("POST /house");
        return houseRepository.save(house);
    }

    @DeleteMapping("/{id}")
    public void deleteHouse(@PathVariable Long id) {
        logger.info("DELETE /houses" + id);
        houseRepository.delete(id);
    }

    @PutMapping
    public House putHouse(@RequestBody House house) {
        logger.info("PUT /house");
        return houseRepository.save(house);
    }

}
