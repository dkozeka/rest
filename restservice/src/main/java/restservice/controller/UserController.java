package restservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import restservice.entities.User;
import restservice.repository.UserRepository;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/{id}")
    public User getHouse(@PathVariable Long id) {
        return userRepository.findOne(id);
    }

    @GetMapping
    public Iterable<User> getHouses() {
        return userRepository.findAll();
    }


    @PostMapping
    public User postHouse(@RequestBody User user) {
        return userRepository.save(user);
    }

    @DeleteMapping("/{id}")
    public void deleteHouse(@PathVariable Long id) {
        userRepository.delete(id);
    }

    @PutMapping
    public User putHouse(@RequestBody User user) {
        return userRepository.save(user);
    }
}
