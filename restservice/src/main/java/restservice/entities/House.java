package restservice.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "house")
public class House {
    private @Id @GeneratedValue Long id;
    private String address;
    private double square;
    private int floors;

    public House() {
    }

    public House(String address, double square, int floors){
        this.address = address;
        this.square = square;
        this.floors = floors;
    }

    public String getAddress() {
        return address;
    }

    public double getSquare() {
        return square;
    }

    public int getFloors() {
        return floors;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setSquare(double square) {
        this.square = square;
    }

    public void setFloors(int floors) {
        this.floors = floors;

    }
}
